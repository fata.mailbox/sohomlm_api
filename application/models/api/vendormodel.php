<?php
class Vendormodel extends CI_Model{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function cekVendor($account)
	{
				
		$query = "SELECT * 
    			 FROM supplier
                 WHERE sap_code = '$account'";
		log_message('INFO','Check query = "'.$query.'"');		 
		$rs = $this->db->query($query);
        
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }

        $result['countResult']=$rs->num_rows();
        $rs->free_result();
		return $result;
	}


	function insertVendor($account,$name,$alamat,$telp,$createdf,$createdby,$npwp)
	{
	   $checkData = $this->cekVendor($account);
	   $result = array();
	    if ($checkData['countResult']==0){
		   $query = "INSERT INTO supplier
					 (
						name,address,telp,created,createdby,npwp, sap_code
					 )
					 VALUES
					 (
					 	'$name', '$alamat', '$telp', '$createdf', '$createdby','$npwp','$account'
					 )
					";
			$rs = $this->db->query($query);
			log_message('INFO','Insert query = "'.$query.'"');		
		}else{
			$query = "UPDATE supplier
					  SET
						name 		='$name',
						address 	='$alamat',
						telp 	  	= '$telp',
						updatedby 	= '$createdby',
						npwp 	  	= '$npwp',	
						updated 	= '$createdf'
					  WHERE sap_code = '$account'
					";
			$rs = $this->db->query($query);
			log_message('INFO','Update query = "'.$query.'"');
		}	
			
			if($rs){
				$result['responseCode']='00';
			}else{
				$result['responseCode']='02';	
			}
		return $result;
        
	}
}

// Annisa Rahmawaty 2019
?>