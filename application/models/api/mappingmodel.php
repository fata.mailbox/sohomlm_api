<?php
class Mappingmodel extends CI_Model{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	function getMappingList($SUB_DIST_MAP_VALUE,$MAPPING_ID,$SUB_DIST_ID){
	   
		$rs = $this->db->query("SELECT * 
    				    FROM ifsapp.CTM_SDR_SUB_DIST_MAPPING_TAB
                        WHERE MAPPING_ID = '$MAPPING_ID' AND SUB_DIST_ID='$SUB_DIST_ID' AND SUB_DIST_MAP_VALUE = '".str_replace("'", "''", $SUB_DIST_MAP_VALUE)."'");
        
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }

        $result['countResult']=$rs->num_rows();
        $rs->free_result();
		return $result;
        
	}
	
	function getMappingCurrent($SUB_DIST_ID,$SUB_DIST_MAP_VALUE){
	   
		$rs = $this->db->query("SELECT * 
    				    FROM ifsapp.CTM_SDR_SUB_DIST_MAPPING_TAB
                        WHERE SUB_DIST_ID = '$SUB_DIST_ID' AND SUB_DIST_MAP_VALUE = '".str_replace("'", "''", $SUB_DIST_MAP_VALUE)."'");
        
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }

        $result['countResult']=$rs->num_rows();
        $rs->free_result();
		return $result;
        
	}

	function getMappingListView($start,$limit)
	{
		$query = '
		SELECT * FROM 
		(
		  SELECT smt.MAPPING_ID,smt.SUB_DIST_ID,smt.SUB_DIST_MAP_VALUE,smt.OUR_MAP_VALUE,smt.ROWVERSION,sdmt.NAME,
		  ROW_NUMBER() OVER (ORDER BY smt.ROWVERSION DESC) R 
		  FROM ifsapp.CTM_SDR_SUB_DIST_MAPPING_TAB smt 
		  LEFT JOIN ifsapp.CTM_SDR_SUB_DIST_MST_TAB sdmt 
		  ON smt.SUB_DIST_ID=sdmt.SUB_DIST_ID
		)		
		';
		if($start!=''&&$limit!=''){
			$end = $start + $limit - 1;
			$wQuery = 'WHERE R BETWEEN '.$start.' and '.$end;
		}else{
			$wQuery = '';
		}
		log_message('INFO',$query.$wQuery);

		$rs = $this->db->query($query.$wQuery);
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }
		$rsTotal = $this->db->query($query);
        $result['countResult']=$rs->num_rows();
		$result['totalRow']=$rsTotal->num_rows();
		
        $rs->free_result();
		return $result;
	}

	function getMappingMstList(){
		$rs = $this->db->query("SELECT * 
    				    FROM ifsapp.CTM_SDR_MAPPING_TAB");
        
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }

        $result['countResult']=$rs->num_rows();
        $rs->free_result();
		return $result;
	}
	
	function checkDataMapping($MAPPING_ID='',$SUB_DIST_ID='',$SUB_DIST_MAP_VALUE='')
	{
	   	if($MAPPING_ID!='')
			$MAPPING_IDQuery = " AND MAPPING_ID = '$MAPPING_ID'";
		else
			$MAPPING_IDQuery = "";
		$rs = $this->db->query("SELECT * 
    				    FROM ifsapp.CTM_SDR_SUB_DIST_MAPPING_TAB
                        WHERE SUB_DIST_ID = '$SUB_DIST_ID' 
						AND SUB_DIST_MAP_VALUE = '".str_replace("'", "''", $SUB_DIST_MAP_VALUE)."'".$MAPPING_IDQuery);
        
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }

        $result['countResult']=$rs->num_rows();
        $rs->free_result();
		return $result;
        
	}
	
	function insertMapping($MAPPING_ID,$SUB_DIST_ID,$SUB_DIST_MAP_VALUE,$OUR_MAP_VALUE,$ROWVERSION){
	   $checkData = $this->checkDataMapping($MAPPING_ID,$SUB_DIST_ID,$SUB_DIST_MAP_VALUE);
	   $result = array();
	   if ($checkData['countResult']==0){
		    $query = "INSERT INTO
							ifsapp.ctm_sdr_sub_dist_mapping_tab
							(
							 	MAPPING_ID,
								SUB_DIST_ID,
								SUB_DIST_MAP_VALUE,
								OUR_MAP_VALUE,
								ROWVERSION
							)
							VALUES
							(
							 	'$MAPPING_ID',
								'$SUB_DIST_ID',
								'".str_replace("'", "''", $SUB_DIST_MAP_VALUE)."',
								'$OUR_MAP_VALUE',
								SYSDATE
							)
					";
			log_message('INFO','Insert Mapping Value query = "'.$query.'"');
			$rs = $this->db->query($query);
			
			if($rs){
				$result['responseCode']='00';
			}else{
				$result['responseCode']='02';	
			}
	   }else{
		   $result['responseCode']='10';	
	   }
		return $result;
        
	}
	
	function updateMapping($MAPPING_ID,$SUB_DIST_ID,$SUB_DIST_MAP_VALUE,$OUR_MAP_VALUE,$ROWVERSION){
	   $checkData = $this->checkDataMapping('',$SUB_DIST_ID,$SUB_DIST_MAP_VALUE);
	   $result = array();
	   if ($checkData['countResult']>0){
		    $query = "UPDATE ifsapp.ctm_sdr_sub_dist_mapping_tab SET
					  MAPPING_ID = '$MAPPING_ID',
					  OUR_MAP_VALUE = '$OUR_MAP_VALUE',
					  ROWVERSION = SYSDATE
					  WHERE SUB_DIST_ID = '$SUB_DIST_ID' AND SUB_DIST_MAP_VALUE = '".str_replace("'", "''", $SUB_DIST_MAP_VALUE)."'
					";
			log_message('INFO','UPDATE Mapping Value query = "'.$query.'"');
			$rs = $this->db->query($query);
			
			if($rs){
				$result['responseCode']='00';
			}else{
				$result['responseCode']='02';	
			}
	   }else{
		   $result['responseCode']='15';	
	   }
		return $result;
        
	}
	
	function checkMappingUse($MAPPING_ID='',$OUR_MAP_VALUE='')
	{
		if($MAPPING_ID!=''){
			if((string)$MAPPING_ID=='CHANNEL'){
				$wQuery = 'WHERE';
				if($OUR_MAP_VALUE!=''){
					$pQuery = "MARKET_CODE = '".(string)$OUR_MAP_VALUE."'";
					$wQuery.= " ".$pQuery; 
				}
				if($wQuery == 'WHERE')$wQuery = "";
				log_message('INFO',"SELECT *
								FROM ifsapp.ctm_sdr_report_customer_tab
								".$wQuery);
				$rs = $this->db->query("SELECT * 
								FROM ifsapp.ctm_sdr_report_customer_tab
								".$wQuery);
				
				$result = array();
				if ($rs->num_rows() > 0) {
					foreach($rs->result_array() as $row ) {
						$result['data'][] = $row;
					}
				}
		
				$result['countResult']=$rs->num_rows();
				$rs->free_result();
			}else if((string)$MAPPING_ID=='PRODUCT'){
				$wQuery = 'WHERE';
				if($OUR_MAP_VALUE!=''){
					$pQuery = "PART_NO = '".(string)$OUR_MAP_VALUE."'";
					$wQuery.= " ".$pQuery; 
				}
				if($wQuery == 'WHERE')$wQuery = "";
				log_message('INFO',"SELECT *
								FROM ifsapp.ctm_sdr_report_transaction_tab
								".$wQuery);
				$rs = $this->db->query("SELECT * 
								FROM ifsapp.ctm_sdr_report_transaction_tab
								".$wQuery);
				
				$result = array();
				if ($rs->num_rows() > 0) {
					foreach($rs->result_array() as $row ) {
						$result['data'][] = $row;
					}
				}
		
				$result['countResult']=$rs->num_rows();
				$rs->free_result();
			}else{
				$result['countResult']=0;
			}
		}else{
			$result['countResult']=0;
		}
		return $result;
        
	}

	
	function deleteMapping($MAPPING_ID,$SUB_DIST_ID,$SUB_DIST_MAP_VALUE,$OUR_MAP_VALUE){
	   $checkData = $this->checkDataMapping('',$SUB_DIST_ID,$SUB_DIST_MAP_VALUE);
	   $result = array();
	   if ($checkData['countResult']>0){
			log_message('INFO','data Mapping for delete ("'.$MAPPING_ID.'","'.$OUR_MAP_VALUE.'")');
			$checkMappingUse = $this->checkMappingUse($MAPPING_ID,$OUR_MAP_VALUE);
	   		if ($checkMappingUse['countResult']==0){
				$query = "DELETE FROM ifsapp.ctm_sdr_sub_dist_mapping_tab 
						  WHERE SUB_DIST_ID = '$SUB_DIST_ID' AND SUB_DIST_MAP_VALUE = '".str_replace("'", "''", $SUB_DIST_MAP_VALUE)."'
						";
				log_message('INFO','DELETE Mapping Value query = "'.$query.'"');
				$rs = $this->db->query($query);
				
				if($rs){
					$result['responseCode']='00';
				}else{
					$result['responseCode']='02';	
				}
			}else{
			   $result['responseCode']='17';	
			}
	   }else{
		   $result['responseCode']='15';	
	   }
		return $result;
        
	}

	function insertitem($item_id, $name, $satuan, $hpp, $sales, $created, $createdby)
	{
	   
	   $result = array();
		   $query = "INSERT INTO item
					 (
						id,name,satuan,hpp,sales,created,createdby
					 )
					 VALUES
					 (
					 	'$item_id', '$name', '$satuan', '$hpp', '$sales', '$created', '$createdby'
					 )
					";
			$rs = $this->db->query($query);
			
			log_message('INFO','Insert query = "'.$query.'"');
			if($rs){
				$result['responseCode']='00';
			}else{
				$result['responseCode']='02';	
			}
		return $result;
        
	}
}
?>