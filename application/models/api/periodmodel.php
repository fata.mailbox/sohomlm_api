<?php
class Periodmodel extends CI_Model{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	function getData($PERIOD_ID='',$FROM_DATE='',$UNTIL_DATE=''){
	   
	    $wQuery = 'WHERE';
		if($PERIOD_ID!=''){
			$pQuery = "PERIOD_ID = '".$PERIOD_ID."'";
			$wQuery.= " ".$pQuery; 
		}
	    if($FROM_DATE!=''){
			$fdQuery = "FROM_DATE = TO_DATE('".$FROM_DATE."','mm/dd/yyyy')";
			if($wQuery != 'WHERE')$andQ = " AND "; else $andQ = " ";
			$wQuery.= $andQ.$fdQuery; 
		}
	    if($UNTIL_DATE!='')
		{
			
			$udQuery = "UNTIL_DATE = TO_DATE('".$UNTIL_DATE."','mm/dd/yyyy')";
			if($wQuery != 'WHERE')$andQ = " AND "; else $andQ = " ";
			$wQuery.= $andQ.$udQuery; 
		}
		if($wQuery == 'WHERE')$wQuery = "";
		log_message('INFO',"SELECT * 
    				    FROM ifsapp.CTM_SDR_REPORT_PERIOD_TAB
                        ".$wQuery);

		$rs = $this->db->query("SELECT * 
    				    FROM ifsapp.CTM_SDR_REPORT_PERIOD_TAB
                        ".$wQuery);
        
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }

        $result['countResult']=$rs->num_rows();
        $rs->free_result();
		return $result;
        
	}
	
	function getPeriodCurrentData($PERIOD_ID=''){
	   
	    $wQuery = 'WHERE';
		if($PERIOD_ID!=''){
			$pQuery = "PERIOD_ID = '".$PERIOD_ID."'";
			$wQuery.= " ".$pQuery; 
		}
		if($wQuery == 'WHERE')$wQuery = "";
		log_message('INFO',"SELECT PERIOD_ID, DESCRIPTION, TO_CHAR(FROM_DATE,'mm/dd/yyyy'),  TO_CHAR(FROM_DATE,'mm/dd/yyyy'), ROWVERSION
    				    FROM ifsapp.CTM_SDR_REPORT_PERIOD_TAB
                        ".$wQuery);

		$rs = $this->db->query("SELECT PERIOD_ID, DESCRIPTION, TO_CHAR(FROM_DATE,'mm/dd/yyyy') AS FROM_DATE,  TO_CHAR(UNTIL_DATE,'mm/dd/yyyy') AS UNTIL_DATE, ROWVERSION
    				    FROM ifsapp.CTM_SDR_REPORT_PERIOD_TAB
                        ".$wQuery);
        
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }

        $result['countResult']=$rs->num_rows();
        $rs->free_result();
		return $result;
        
	}

	function insertPeriod($PERIOD_ID,$DESCRIPTION,$FROM_DATE,$UNTIL_DATE,$ROWVERSION){
	   $checkData = $this->getData('',$FROM_DATE,$UNTIL_DATE);
	   $result = array();
	   if ($checkData['countResult']==0){
		    $query = "INSERT INTO
							ifsapp.CTM_SDR_REPORT_PERIOD_TAB
							(
							 	PERIOD_ID,
								DESCRIPTION,
								FROM_DATE,
								UNTIL_DATE,
								ROWVERSION
							)
							VALUES
							(
							 	'$PERIOD_ID',
								'$DESCRIPTION',
								TO_DATE('$FROM_DATE','mm/dd/yyyy'),
								TO_DATE('$UNTIL_DATE','mm/dd/yyyy'),
								SYSDATE
							)
					";
			log_message('INFO','Insert Period query = "'.$query.'"');
			$rs = $this->db->query($query);
			
			if($rs){
				$result['responseCode']='00';
			}else{
				$result['responseCode']='02';	
			}
	   }else{
		   $result['responseCode']='10';	
	   }
		return $result;
        
	}
	
	function updatePeriod($PERIOD_ID,$DESCRIPTION,$FROM_DATE,$UNTIL_DATE,$ROWVERSION){
	   $checkData = $this->getData($PERIOD_ID,'','');
	   $result = array();
	   if ($checkData['countResult']>0){
		    $query = "UPDATE ifsapp.CTM_SDR_REPORT_PERIOD_TAB
					  SET
						DESCRIPTION='$DESCRIPTION',
						FROM_DATE=TO_DATE('$FROM_DATE','mm/dd/yyyy'),
						UNTIL_DATE=TO_DATE('$UNTIL_DATE','mm/dd/yyyy'),
						ROWVERSION=SYSDATE
					  WHERE
						PERIOD_ID='$PERIOD_ID'
					";
			log_message('INFO','Update Period query = "'.$query.'"');
			$rs = $this->db->query($query);
			
			if($rs){
				$result['responseCode']='00';
			}else{
				$result['responseCode']='02';	
			}
	   }else{
		   $result['responseCode']='15';	
	   }
		return $result;
        
	}
	
	function checkPeriodUse($PERIOD_ID){
	    $wQuery = 'WHERE';
		if($PERIOD_ID!=''){
			$pQuery = "PERIOD_ID = '".$PERIOD_ID."'";
			$wQuery.= " ".$pQuery; 
		}
		if($wQuery == 'WHERE')$wQuery = "";
		log_message('INFO',"SELECT *
    				    FROM ifsapp.ctm_sdr_period_status_tab
                        ".$wQuery);

		$rs = $this->db->query("SELECT *
    				    FROM ifsapp.ctm_sdr_period_status_tab
                        ".$wQuery);
        
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }

        $result['countResult']=$rs->num_rows();
        $rs->free_result();
		return $result;
	}
	
	function deletePeriod($PERIOD_ID){
	   $checkData = $this->getData($PERIOD_ID,'','');
	   $result = array();
	   if ($checkData['countResult']>0){
		   $checkperioduse=$this->checkPeriodUse($PERIOD_ID);
		   if($checkperioduse['countResult']==0){
				$query = "
						  DELETE FROM ifsapp.CTM_SDR_REPORT_PERIOD_TAB
						  WHERE PERIOD_ID='$PERIOD_ID'
						 ";
				log_message('INFO','Delete Period query = "'.$query.'"');
				$rs = $this->db->query($query);
				
				if($rs){
					$result['responseCode']='00';
				}else{
					$result['responseCode']='02';	
				}
		   }else{
			   $result['responseCode']='17';	
		   }
	   }else{
		   $result['responseCode']='15';	
	   }
		return $result;
        
	}

	
	function getPeriodList($start,$limit)
	{
		$query = '
		SELECT * FROM 
		(
		  SELECT PERIOD_ID,DESCRIPTION,FROM_DATE,UNTIL_DATE,ROWVERSION,
		  ROW_NUMBER() OVER (ORDER BY FROM_DATE DESC) R 
		  FROM ifsapp.ctm_sdr_report_period_tab
		) 
		';
		if($start!=''&&$limit!=''){
			$end = $start + $limit - 1;
			$wQuery = 'WHERE R BETWEEN '.$start.' and '.$end;
		}else{
			$wQuery = '';
		}
		log_message('INFO',$query.$wQuery);

		$rs = $this->db->query($query.$wQuery);
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }
		$rsTotal = $this->db->query($query);
        $result['countResult']=$rs->num_rows();
		$result['totalRow']=$rsTotal->num_rows();
		
        $rs->free_result();
		return $result;
	}
	
	function validatePeriod($PERIOD_ID,$DATE_TO_VALIDATE)
	{
		$query = "
		select count(*) as STATVAL 
		from ifsapp.ctm_sdr_report_period_tab t 
		where 
		until_date >= to_date('$DATE_TO_VALIDATE','dd/mm/yyyy') 
		and from_date <= to_date('$DATE_TO_VALIDATE','dd/mm/yyyy') 
		and period_id ='$PERIOD_ID'
		";
		log_message('INFO',$query);

		$rs = $this->db->query($query);
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result() as $row ) {
                $result['STATVAL'] = $row->STATVAL;
            }
        }
		return $result;
	}
	
	function getPeriodReport($month='', $year=''){
	   
	    $wQuery = 'WHERE';
		if($month!=''){
			$mQuery = "TO_CHAR(FROM_DATE, 'mm') = '".$month."'";
			$wQuery.= " ".$mQuery; 
		}
		if($year!=''){
			$yQuery = "TO_CHAR(FROM_DATE, 'yyyy') = '".$year."'";
			if($wQuery != 'WHERE')$andQ = " AND "; else $andQ = " ";
			$wQuery.= $andQ.$yQuery; 
		}
		if($wQuery == 'WHERE')$wQuery = "";
		
		$Query = "SELECT * 
				 FROM ifsapp.CTM_SDR_REPORT_PERIOD_TAB
				 ".$wQuery." ORDER BY FROM_DATE ASC";
				 
		log_message('INFO',$Query);

		$rs = $this->db->query($Query);
        
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }

        $result['countResult']=$rs->num_rows();
        $rs->free_result();
		return $result;
        
	}
}
?>