<?php
class Transactionreport extends CI_Model{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	function checkData($PERIOD_ID, $SUB_DIST_ID, $SUBMIT_DATE='', $INVOICE_NO='', $PART_NO=''){
	    if($SUBMIT_DATE!=''){
			$wQuery = " AND SUBMIT_DATE = TO_DATE('$SUBMIT_DATE','yyyymmdd')";	
		}else{
			$wQuery = "";
		}
	    if($INVOICE_NO!=''){
			$wQuery.= " AND INVOICE_NO = '$INVOICE_NO'";	
		}else{
			$wQuery.= "";
		}
	    if($PART_NO!=''){
			$wQuery.= " AND PART_NO = '$PART_NO'";	
		}else{
			$wQuery.= "";
		}

		$rs = $this->db->query("SELECT * 
    				    FROM ifsapp.CTM_SDR_REPORT_TRANSACTION_TAB
                        WHERE PERIOD_ID = '$PERIOD_ID' AND SUB_DIST_ID = '$SUB_DIST_ID'".$wQuery);// AND SUBMIT_DATE = TO_DATE('$SUBMIT_DATE','yyyymmdd') AND INVOICE_NO = '$INVOICE_NO' AND PART_NO='$PART_NO'");
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }

        $result['countResult']=$rs->num_rows();
        $rs->free_result();
		return $result;
        
	}
	
	function insertTransaction($PERIOD_ID,$SUB_DIST_ID,$SUBMIT_DATE,$INVOICE_NO,$INVOICE_DATE,$CUSTOMER_NO,$PART_NO,$UNIT_MEAS, $QTY_INVOICED, $SALE_UNIT_PRICE, $NET_VALUE, $QTY_BONUS, $DISC_BONUS_BARANG, $DISC_REGULER, $DISC_PROGRAM, $ROWVERSION)
	{
	   $checkData = $this->checkData($PERIOD_ID, $SUB_DIST_ID, $SUBMIT_DATE, $INVOICE_NO, $PART_NO);
	   $result = array();
	   if ($checkData['countResult']==0){
		   $query = "INSERT 
							INTO ifsapp.CTM_SDR_REPORT_TRANSACTION_TAB
							(
							 	PERIOD_ID,
								SUB_DIST_ID,
								SUBMIT_DATE,
								INVOICE_NO,
								INVOICE_DATE,
								CUSTOMER_NO,
								PART_NO,
								UNIT_MEAS,
								QTY_INVOICED,
								SALE_UNIT_PRICE,
								NET_VALUE,
								QTY_BONUS,
								ROWVERSION,
								DISC_REGULER,
								DISC_PROGRAM,
								DISC_BONUS_BARANG
							)
							VALUES
							(
							 	'$PERIOD_ID',
								'$SUB_DIST_ID',
								TO_DATE('$SUBMIT_DATE','yyyymmdd'),
								'$INVOICE_NO',
								TO_DATE('$INVOICE_DATE','dd/mm/yyyy'),
								'$CUSTOMER_NO',
								'$PART_NO',
								'$UNIT_MEAS',
								$QTY_INVOICED,
								$SALE_UNIT_PRICE,
								$NET_VALUE,
								$QTY_BONUS,
								SYSDATE,
								$DISC_PROGRAM,
								$DISC_REGULER,
								$DISC_BONUS_BARANG
							 )
							";
			$rs = $this->db->query($query);
			
			log_message('INFO','Insert TRANSACTION query = "'.$query.'"');
			if($rs){
				$result['responseCode']='00';
			}else{
				$result['responseCode']='02';	
			}
	   }else{
		   $result['responseCode']='10';	
	   }
		return $result;
        
	}
}
?>