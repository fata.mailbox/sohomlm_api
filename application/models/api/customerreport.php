<?php
class Customerreport extends CI_Model{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	function checkData($PERIOD_ID,$SUB_DIST_ID,$SUBMIT_DATE='',$CUSTOMER_NO=''){
	    if($SUBMIT_DATE!=''){
			$wQuery = " AND SUBMIT_DATE = TO_DATE('$SUBMIT_DATE','yyyymmdd')";	
		}else{
			$wQuery = "";
		}
	    if($CUSTOMER_NO!=''){
			$wQuery.= " AND CUSTOMER_NO = '$CUSTOMER_NO'";	
		}else{
			$wQuery.= "";
		}
		
		$query = "SELECT * 
    			 FROM ifsapp.CTM_SDR_REPORT_CUSTOMER_TAB
                 WHERE PERIOD_ID = '$PERIOD_ID' AND SUB_DIST_ID = '$SUB_DIST_ID'".$wQuery;//." AND CUSTOMER_NO = '$CUSTOMER_NO'";
		log_message('INFO','Check query = "'.$query.'"');		 
		$rs = $this->db->query($query);
        
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }

        $result['countResult']=$rs->num_rows();
        $rs->free_result();
		return $result;
        
	}
	
	function insertCust($PERIOD_ID,$SUB_DIST_ID,$SUBMIT_DATE,$CUSTOMER_NO,$CUSTOMER_NAME,$MARKET_CODE,$ADDRESS,$COUNTY,$STATE,$PHONE_NO,$FAX_NO,$ZIP_CODE,$ROWVERSION)
	{
	   $checkData = $this->checkData($PERIOD_ID,$SUB_DIST_ID,$SUBMIT_DATE,$CUSTOMER_NO);
	   $result = array();
	   if ($checkData['countResult']==0){
		   $query = "INSERT 
							INTO ifsapp.CTM_SDR_REPORT_CUSTOMER_TAB
							(
							 	PERIOD_ID,
								SUB_DIST_ID,
								SUBMIT_DATE,
								CUSTOMER_NO,
								CUSTOMER_NAME,
								MARKET_CODE,
								ADDRESS,
								COUNTY,
								STATE,
								PHONE_NO,
								FAX_NO,
								ZIP_CODE,
								ROWVERSION
							)
							VALUES
							(
							 	'$PERIOD_ID',
								'$SUB_DIST_ID',
								TO_DATE('$SUBMIT_DATE','yyyymmdd'),
								'$CUSTOMER_NO',
								'".str_replace("'", "''", $CUSTOMER_NAME)."',
								'$MARKET_CODE',
								'".str_replace("'", "''", $ADDRESS)."',
								'$COUNTY',
								'$STATE',
								'$PHONE_NO',
								'$FAX_NO',
								'$ZIP_CODE',
								SYSDATE
							 )
							";
			$rs = $this->db->query($query);
			
			log_message('INFO','Insert query = "'.$query.'"');
			if($rs){
				$result['responseCode']='00';
			}else{
				$result['responseCode']='02';	
			}
	   }else{
		   $result['responseCode']='10';	
	   }
		return $result;
        
	}
}
?>