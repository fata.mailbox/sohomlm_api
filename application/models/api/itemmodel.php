<?php
class Itemmodel extends CI_Model{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function cekItemId($item_id)
	{
				
		$query = "SELECT * 
    			 FROM item
                 WHERE id = '$item_id'";
		log_message('INFO','Check query = "'.$query.'"');		 
		$rs = $this->db->query($query);
        
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }

        $result['countResult']=$rs->num_rows();
        $rs->free_result();
		return $result;
	}

	function insertitem($item_id,$name,$satuan,$hpp,$sales,$createdf,$createdby,$material,$purchasing,$updatedf,$updateby,$price,$price2)
	{
	   if($price == '') $price = 0;
	   if($price2 == '') $price2 = 0;
	   $checkData = $this->cekItemId($item_id);
	   $result = array();
	    if ($checkData['countResult']==0){
			if($purchasing=='UGM'||$purchasing=='UTP'||$purchasing=='UTH'||$purchasing=='UPM') $typeid = 3;
			else if($purchasing=='USK') $typeid = 1;
			else $typeid = 2;
			
		   $query = "INSERT INTO item
					 (
						id,name,satuan,hpp,sales,created,createdby,material_group,purchasing_group
						,warehouse_id, manufaktur, display, price, price2, pv, bv, type_id
					 )
					 VALUES
					 (
					 	'$item_id', '$name', '$satuan', '$hpp', '$sales', '$createdf', '$createdby','$material','$purchasing'
						,1, 'No', 'No', $price, $price2, 0, 0, '$typeid'
					 )
					";
			$rs = $this->db->query($query);
			$this->db->query("call sp_item('$item_id')");
			log_message('INFO','Insert query = "'.$query.'"');
		}else{
			$query = "UPDATE item
					  SET
						name ='$name',
						satuan ='$satuan',
						hpp = '$hpp',
						price = '$price',
						price2 = '$price2',
						sales = '$sales',
						updatedby = '$updateby',
						updated = '$updatedf',
						material_group = '$material',
						purchasing_group = '$purchasing',
						type_id = '$typeid'
					  	WHERE id = '$item_id'
					";
			$rs = $this->db->query($query);
			log_message('INFO','Update query = "'.$query.'"');
		}
			
			
			if($rs){
				$result['responseCode']='00';
			}else{
				$result['responseCode']='02';	
			}
		return $result;
        
	}

	//Annisa Rahmawaty 2019
	function updatehna($item_id,$price,$validfrom,$REGIO,$updatedby){
	   if($price == '') $price = 0;
	   $qprice = '';
	   
	   if($REGIO=='U2')$qprice = 'price='.$price;
	   else if($REGIO=='U1')$qprice = 'price2='.$price;

	   
	   $checkData = $this->cekItemId($item_id);
	   $result = array();
	   if($qprice!=''){
			if ($checkData['countResult']>0){
				$query = "UPDATE item
						  SET
						  ".$qprice."
						  ,updated = NOW()
						  ,updatedby = '".$updatedby."'
						  WHERE id = '$item_id'
						";
				$rs = $this->db->query($query);
				log_message('INFO','Update query = "'.$query.'"');
				
				if($rs){
					$result['responseCode']='00';
				}else{
					$result['responseCode']='02';	
				}
			}else{
				$result['responseCode']='03';
			}
		}else{
			$result['responseCode']='04';
		}
		
		return $result;
	}
}
?>