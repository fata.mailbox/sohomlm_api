<?php
class Periodstatusmodel extends CI_Model{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	function checkData($PERIOD_ID,$SUB_DIST_ID,$REPORT_TYPE){
	   	$query = "SELECT * 
    			 FROM ifsapp.CTM_SDR_PERIOD_STATUS_TAB
                 WHERE PERIOD_ID = '$PERIOD_ID' AND SUB_DIST_ID = '$SUB_DIST_ID' AND REPORT_TYPE = '$REPORT_TYPE'";
		log_message('INFO','Check Period Status query = "'.$query.'"');
		$rs = $this->db->query($query);
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }
        $result['countResult']=$rs->num_rows();
		log_message('INFO','Check Period Status num rows = "'.$result['countResult'].'"');
        $rs->free_result();
		return $result;
        
	}
	function insertPeriodStatus($PERIOD_ID,$SUB_DIST_ID,$REPORT_TYPE,$SUBMIT_DATE,$SUBMIT_BY,$ROWVERSION){
	   $checkData = $this->checkData($PERIOD_ID,$SUB_DIST_ID,$REPORT_TYPE);
	   $result = array();
	   if ($checkData['countResult']==0){
		    $query = "INSERT INTO
							ifsapp.CTM_SDR_PERIOD_STATUS_TAB
							(
							 	PERIOD_ID,
								SUB_DIST_ID,
								REPORT_TYPE,
								SUBMIT_DATE,
								SUBMIT_BY,
								ROWVERSION
							)
							VALUES
							(
							 	'$PERIOD_ID',
								'$SUB_DIST_ID',
								'$REPORT_TYPE',
								TO_DATE('$SUBMIT_DATE','yyyymmdd'),
								'$SUBMIT_BY',
								TO_DATE('$ROWVERSION','yyyymmdd')
							)
					";
			log_message('INFO','Insert Period Status query = "'.$query.'"');
			$rs = $this->db->query($query);
			
			if($rs){
				$result['responseCode']='00';
			}else{
				$result['responseCode']='02';	
			}
	   }else{
		   $result['responseCode']='10';	
	   }
		return $result;
        
	}
}
?>