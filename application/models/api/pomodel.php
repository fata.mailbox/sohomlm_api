<?php
class Pomodel extends CI_Model{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function cekPoId($po_id)
	{
				
		$query = "SELECT * 
    			 FROM po_order
                 WHERE sap_code = '$po_id'";
		log_message('INFO','Check query = "'.$query.'"');		 
		$rs = $this->db->query($query);
        
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }

        $result['countResult']=$rs->num_rows();
        $rs->free_result();
		return $result;
	}

	function cekSupplier($supplier)
	{
				
		$query = "SELECT * 
    			 FROM supplier
                 WHERE sap_code = '$supplier'";
		log_message('INFO','Check query = "'.$query.'"');		 
		$rs = $this->db->query($query);
        
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }

        $result['countResult']=$rs->num_rows();
        $rs->free_result();
		return $result;
	}


	function cekPoId_d($po_id,$line)
	{
				
		$query = "SELECT * 
    			 FROM po_order_d
                 WHERE po_sap_code = '$po_id' and po_line_sap_code = '$line'";
		log_message('INFO','Check query = "'.$query.'"');		 
		$rs = $this->db->query($query);
        
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }

        $result['countResult']=$rs->num_rows();
        $rs->free_result();
		return $result;
	}
	
	function cekActualPoId($po_id,$tahun)
	{
				
		$query = "SELECT * 
    			 FROM po
                 WHERE sap_code = '$po_id' and YEAR(tgl) = '$tahun'";
		log_message('INFO','Check query = "'.$query.'"');		 
		$rs = $this->db->query($query);
        
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }

        $result['countResult']=$rs->num_rows();
        $rs->free_result();
		return $result;
	}
	
	function cekActualPoId_d($po_id)
	{
				
		$query = "SELECT * 
    			 FROM po_d
                 WHERE po_sap_code = '$po_id'";
		log_message('INFO','Check query = "'.$query.'"');		 
		$rs = $this->db->query($query);
        
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }

        $result['countResult']=$rs->num_rows();
        $rs->free_result();
		return $result;
	}

	function insertPo($po_id,$createdf,$supplier,$line,$wh,$item_id,$qty,$price,$subtotal,$sloc,$createdby,$createdftime)
	{

	   	$checkData = $this->cekPoId($po_id);
	   	$cekdetail = $this->cekPoId_d($po_id,$line);
		$cekSupplier = $this->cekSupplier($supplier);
		$error = FALSE;
		$errMsg = '';

		if($cekSupplier['countResult'] < 1) {
			$error = TRUE;
			$errMsg = $errMsg.'|Supplier Not Found';
		}
		$errMsg = trim($errMsg ,'|');
	   	$query = "SELECT * FROM warehouse WHERE sap_code = '$wh' ";
	   	 //echo $query ;
		$q = $this->db->query($query)->row()->id;
		
		

	  	$result = array();
		if($error==FALSE){
			$querysupplier = "SELECT * FROM supplier WHERE sap_code = '$supplier' ";
			 //echo $query ;
			$qquerysupplier = $this->db->query($querysupplier)->row()->id;

			if ($checkData['countResult'] < 1){
				if ($q == 1) {
					$sl = substr($sloc,3,1);
		    		$query = "INSERT INTO po_order
						 (
							sap_code,tgl,supplier_id,warehouse_id,created,createdby,totalorder
						 )
						 VALUES
						 (
						 	'$po_id','$createdf','$qquerysupplier','$sl','$createdftime','$createdby','$subtotal'
						 )
						";
				$rs = $this->db->query($query);
				$id = $this->db->insert_id();
				//echo $query ;
				log_message('INFO','Insert query = "'.$query.'"');
		    	}else{
			   		$query = "INSERT INTO po_order
						 (
							sap_code,tgl,supplier_id,warehouse_id,created,createdby,totalorder
						 )
						 VALUES
						 (
						 	'$po_id','$createdf','$supplier','$q','$createdftime','$createdby','$subtotal'
						 )
						";
				$rs = $this->db->query($query);
				$id = $this->db->insert_id();
				log_message('INFO','Insert query = "'.$query.'"');
				}
				if ($cekdetail['countResult'] < 1) {
					$query2 = "INSERT INTO po_order_d
						 (
							po_order_id, po_sap_code, po_line_sap_code,item_id,qty,harga,jmlharga
						 )
						 VALUES
						 (
						 	$id,'$po_id', '$line','$item_id','$qty','$price','$subtotal'
						 )
						";
				$q2 = $this->db->query($query2);
				log_message('INFO','Insert query = "'.$query2.'"');

				}else{
					$query2 = "UPDATE po_order_d
							  SET
								item_id 	= '$item_id',
								qty 		= '$qty',
								harga 	 	= '$price',
								jmlharga 	= '$subtotal'
							  WHERE po_line_sap_code = '$line' and po_sap_code = '$po_id'
							";
					$rs = $this->db->query($query);
					log_message('INFO','Update query = "'.$query2.'"');
				}
			}else{
				if ($cekdetail['countResult'] < 1) {
					$queryGetPoId = "SELECT * FROM po_order WHERE sap_code = '$po_id'";
					 //echo $query ;
					$id = $this->db->query($queryGetPoId)->row()->id;
					$query2 = "INSERT INTO po_order_d
						 (
							po_order_id, po_sap_code, po_line_sap_code,item_id,qty,harga,jmlharga
						 )
						 VALUES
						 (
						 	$id,'$po_id', '$line','$item_id','$qty','$price','$subtotal'
						 )
						";
					$rs = $this->db->query($query2);
					$updatetotalorder = $this->db->query("UPDATE po_order set totalorder = totalorder+".$subtotal." where id = ".$id);

				log_message('INFO','Insert query = "'.$query2.'"');
				}else{
					$query2 = "UPDATE po_order_d
							  SET
								item_id 	= '$item_id',
								qty 		= '$qty',
								harga 	 	= '$price',
								jmlharga 	= '$subtotal'
							  WHERE po_line_sap_code = '$line' and po_sap_code = '$po_id'
							";
					$rs = $this->db->query($query);
					log_message('INFO','Update query = "'.$query2.'"');
				}
			}
			if($rs){
				$result['responseCode']='00';
			}else{
				$result['responseCode']='02';	
			}
		}else{
			log_message('INFO',$errMsg);
			$result['responseCode']='03';
		}
		return $result;
        
	}

	function insertActualPo($po_id,$line,$supplier,$plan,$sloc,$item_id,$name,$qty,$price,$createdby,$createdf,$createdftime,$subtotal,$gr_id,$tahun)
	{
	   	$checkData = $this->cekPoId($po_id);
	   	$checkActualData = $this->cekActualPoId($gr_id,$tahun);
	   	$cekdetail = $this->cekActualPoId_d($gr_id);
		$result = array();
		if ($checkData['countResult'] > 0){
			if($checkActualData['countResult'] < 1){
			   $queryGetPoId = "SELECT * FROM po_order WHERE sap_code = '$po_id' ";
			   //echo $query ;
			   $poid = $this->db->query($queryGetPoId)->row()->id;
			   $query = "INSERT INTO po
						 (
							po_order_id,tgljatuhtempo,tgl,created,createdby,remark,sap_code,totalactual
						 )
						 VALUES
						 (
							'$poid', '$createdf', '$createdf', '$createdf','$createdby','GR From SAP','$gr_id','$subtotal'
						 )
						";
				$rs = $this->db->query($query);
				$id = $this->db->insert_id();
				log_message('INFO','Insert query = "'.$query.'"');
				
				$query2 = "INSERT INTO po_d
						 (
							po_id,item_id,qty,harga, jmlharga, po_sap_code, po_line_sap_code
						 )
						 VALUES
						 (
							'$id', '$item_id', '$qty','$price','$subtotal','$gr_id','$line'
						 )
						";
				$q2 = $this->db->query($query2);
				$id_detail = $this->db->insert_id();
				log_message('INFO','Insert query = "'.$query2.'"');
			}else{
			   $queryGetPoId = "SELECT * FROM po WHERE sap_code = '$gr_id' and YEAR(tgl) = '$tahun'";
			   //echo $query ;
			   $id = $this->db->query($queryGetPoId)->row()->id;
				$query2 = "INSERT INTO po_d
						 (
							po_id,item_id,qty,harga, jmlharga, po_sap_code, po_line_sap_code
						 )
						 VALUES
						 (
							'$id', '$item_id', '$qty','$price','$subtotal','$gr_id','$line'
						 )
						";
				$rs = $this->db->query($query2);
				$id_detail = $this->db->insert_id();
				$updatetotalorder = $this->db->query("UPDATE po set totalactual = totalactual+".$subtotal." where id = ".$id);
				log_message('INFO','Insert query = "'.$query2.'"');
			}
			if($rs){
       			$this->db->query("call sp_po_detail('$id','$id_detail','$createdby')");
				$result['responseCode']='00';
			}else{
				$result['responseCode']='02';	
			}
		}else{
			$result['responseCode']='03';	
		}
		return $result;
	}
}
/*
	function insertPodel($po_id,$createdf,$supplier,$wh,$sloc,$createdby)
	{
	   $checkData = $this->cekPoId($po_id);
	   $query = "SELECT * FROM warehouse WHERE sap_code = '$wh' ";
	   	
		$q = $this->db->query($query)->row()->id;

	   $result = array();
	    if ($checkData['countResult'] = 0){
		   if ($q == 1) {
				$sl = substr($sloc,3,1);
	    		$query = "INSERT INTO po_order
					 (
						id,tgl,supplier_id,warehouse_id,created,createdby
					 )
					 VALUES
					 (
					 	'$po_id','$createdf','$supplier','$sl','$createdf','$createdby'
					 )
					";
			$rs = $this->db->query($query);
			//echo $query ;
			log_message('INFO','Insert query = "'.$query.'"');
	    	}else{
		   		$query = "INSERT INTO po_order
					 (
						id,tgl,supplier_id,warehouse_id,created,createdby
					 )
					 VALUES
					 (
					 	'$po_id','$createdf','$supplier','$q','$createdf','$createdby'
					 )
					";
			$rs = $this->db->query($query);
			log_message('INFO','Insert query = "'.$query.'"');
			}

		}else {
			if ($q == 1) {
				$sl = substr($sloc, 3);
				//echo $sl;
				$query = "UPDATE po_order
						  SET
							tgl 		='$createdf',
							supplier_id	= '$supplier',
							warehouse_id= '$sl',
							updatedby 	= '$createdby',
							updated 	= '$createdf'
						  WHERE id = '$po_id'
						";
				$rs = $this->db->query($query);
				log_message('INFO','Update query = "'.$query.'"');
			}else{
				$query = "UPDATE po_order
						  SET
							tgl 		='$createdf',
							supplier_id	= '$supplier',
							warehouse_id= '$q',
							updatedby 	= '$createdby',
							updated 	= '$createdf'
						  WHERE id = '$po_id'
						";
				$rs = $this->db->query($query);
				log_message('INFO','Update query = "'.$query.'"');
			}
		}
			
			if($rs){
				$result['responseCode']='00';
			}else{
				$result['responseCode']='02';	
			}
		return $result;
        
	}
*/
// Annisa Rahmawaty 2019
?>