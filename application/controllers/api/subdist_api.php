<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Subdist_api extends REST_Controller
{
    public function __Construct()
    {
   	   parent::__Construct();
       //constructor code
	   	$this->load->model('Usermodel');	
		$this->load->model('Customerreport');	
		$this->load->model('Inventoryreport');	
		$this->load->model('Transactionreport');	
		$this->load->model('Periodmodel');	
		$this->load->model('Mappingmodel');	
		$this->load->model('Periodstatusmodel');	
		$this->load->model('Subdistmodel');	
    }
	function userauth_get()
    {
        
        //$this->checkAuth();
        
		//$this->load->model('Usermodel');	
		$data = $this->Usermodel->getData($this->get('uname'),$this->get('upass'));
        
    	
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'User account could not be found'), 404);
		}
    }

    function userauth_post()
    {
		//$this->load->model('Usermodel');	
        log_message('INFO','Incoming request on userauth_post. uname = "'.(string)$this->post('uname').'"; upass = "'.(string)$this->post('upass').'". From IP '.$_SERVER['REMOTE_ADDR']);
		
		$data = $this->Usermodel->getData($this->post('uname'),$this->post('upass'));
    	
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'User account could not be found'), 404);
		}
    }

	function userchangepass_post()
	{
		//$this->load->model('Usermodel');	
        log_message('INFO','Incoming request on userchangepass_post. uname = "'.(string)$this->post('uname').'"; oldpass = "'.(string)$this->post('uoldpass').'"; newpass="'.(string)$this->post('unewpass').'". From IP '.$_SERVER['REMOTE_ADDR']);
		
		$data = $this->Usermodel->updatePass($this->post('uname'),$this->post('uoldpass'),$this->post('unewpass'));
    	
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'User Password could not be changed'), 404);
		}
	}
	
	function getuserlistview_post()
	{
        log_message('INFO','Incoming request on userlistview_post. From IP '.$_SERVER['REMOTE_ADDR']);
		$data = $this->Usermodel->getUserListView($this->post('START'),$this->POST('LIMIT'));
        
    	
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'User account could not be found'), 404);
		}
	}
	
	function getuserdata_post()
	{
        log_message('INFO','Incoming request on getuserdata_post. From IP '.$_SERVER['REMOTE_ADDR']);
		$data = $this->Usermodel->getUserData($this->post('USER_ID'));
        
    	
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'User account could not be found'), 404);
		}
	}
	
	function insertCustomer_post(){
		//$this->load->model('Usermodel');	
        log_message('INFO','Incoming request on insertCustomer_post. From IP '.$_SERVER['REMOTE_ADDR']);
		log_message('INFO','Incoming request on insertCustomer_post. data = "'.$this->post('MARKET_CODE').'". From IP '.$_SERVER['REMOTE_ADDR']);
		
		$data = $this->Customerreport->insertCust($this->post('PERIOD_ID'),$this->post('SUB_DIST_ID'),$this->post('SUBMIT_DATE'),$this->post('CUSTOMER_NO'),$this->post('CUSTOMER_NAME'),$this->post('MARKET_CODE'),$this->post('ADDRESS'),$this->post('COUNTY'),$this->post('STATE'),$this->post('PHONE_NO'),$this->post('FAX_NO'),$this->post('ZIP_CODE'),$this->post('ROWVERSION'));
    	
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'Data Cannot Be Insert'), 404);
		}
	}
	
	function checkCustomerNo_post(){
        log_message('INFO','Incoming request on checkCustomerNo_post. From IP '.$_SERVER['REMOTE_ADDR']);
		
		$data = $this->Customerreport->checkData($this->post('PERIOD_ID'),$this->post('SUB_DIST_ID'),'',$this->post('CUSTOMER_NO'));
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'Data could not be found'), 404);
		}
		
	}

	function getCustomerReport_post(){
        log_message('INFO','Incoming request on getCustomerReport_post. From IP '.$_SERVER['REMOTE_ADDR']);
		
		$data = $this->Customerreport->checkData($this->post('PERIOD_ID'),$this->post('SUB_DIST_ID'),'','');
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'Data could not be found'), 404);
		}
		
	}
	
	function getInventoryReport_post(){
        log_message('INFO','Incoming request on getInventoryReport_post. From IP '.$_SERVER['REMOTE_ADDR']);
		
		$data = $this->Inventoryreport->checkData($this->post('PERIOD_ID'),$this->post('SUB_DIST_ID'),'','');
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'Data could not be found'), 404);
		}
		
	}

	function getTransactionReport_post(){
        log_message('INFO','Incoming request on getTransactionReport_post. From IP '.$_SERVER['REMOTE_ADDR']);
		
		$data = $this->Transactionreport->checkData($this->post('PERIOD_ID'),$this->post('SUB_DIST_ID'),'','','');
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'Data could not be found'), 404);
		}
		
	}

	function insertInventory_post(){
        log_message('INFO','Incoming request on insertInventory_post. From IP '.$_SERVER['REMOTE_ADDR']);
    	log_message('INFO','Incoming request on insertInventory_post. data = "'.','.$this->post('SUB_DIST_ID').','.$this->post('SUBMIT_DATE').','.$this->post('PART_NO').','.$this->post('QTY_ONHAND').','.$this->post('QTY_SALEABLE').','.$this->post('QTY_NOT_SALEABLE').','.$this->post('ROWVERSION').'"');
			
		$data = $this->Inventoryreport->insertInventory($this->post('PERIOD_ID'),$this->post('SUB_DIST_ID'),$this->post('SUBMIT_DATE'),$this->post('PART_NO'),$this->post('QTY_ONHAND'),$this->post('QTY_SALEABLE'),$this->post('QTY_NOT_SALEABLE'),$this->post('ROWVERSION'));
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'Data Cannot Be Insert'), 404);
		}
	}
	
	function insertTransaction_post(){
        log_message('INFO','Incoming request on insertTransaction_post. From IP '.$_SERVER['REMOTE_ADDR']);
    	log_message('INFO','Incoming request on insertTransaction_post. data = "'.','.$this->post('SUB_DIST_ID').','.$this->post('SUBMIT_DATE').','.$this->post('INVOICE_NO').','.$this->post('INVOICE_DATE').','.$this->post('CUSTOMER_NO').','.$this->post('PART_NO').','.$this->post('UNIT_MEAS').','.$this->post('QTY_INVOICED').','.$this->post('SALE_UNIT_PRICE').','.$this->post('NET_VALUE').','.$this->post('QTY_BONUS').','.$this->post('DISC_BONUS_BARANG').','.$this->post('DISC_REGULER').','.$this->post('DISC_PROGRAM').','.$this->post('ROWVERSION').'"');
			
		$data = $this->Transactionreport->insertTransaction($this->post('PERIOD_ID'),$this->post('SUB_DIST_ID'),$this->post('SUBMIT_DATE'),$this->post('INVOICE_NO'),$this->post('INVOICE_DATE'),$this->post('CUSTOMER_NO'),$this->post('PART_NO'),$this->post('UNIT_MEAS'),$this->post('QTY_INVOICED'),$this->post('SALE_UNIT_PRICE'),$this->post('NET_VALUE'),$this->post('QTY_BONUS'),$this->post('DISC_BONUS_BARANG'),$this->post('DISC_REGULER'),$this->post('DISC_PROGRAM'),$this->post('ROWVERSION'));
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'Data Cannot Be Insert'), 404);
		}
	}
	
	function insertPeriod_post(){
        log_message('INFO','Incoming request on insertPeriod_post. From IP '.$_SERVER['REMOTE_ADDR']);
    	
		log_message('INFO','Incoming request on insertPeriod_post. data = "'.','.$this->post('PERIOD_ID').','.$this->post('DESCRIPTION').','.$this->post('FROM_DATE').','.$this->post('UNTIL_DATE').','.$this->post('ROWVERSION').'"');
		$data = $this->Periodmodel->insertPeriod($this->post('PERIOD_ID'),$this->post('DESCRIPTION'),$this->post('FROM_DATE'),$this->post('UNTIL_DATE'),$this->post('ROWVERSION'));
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'Data Cannot Be Insert'), 404);
		}
	}
	
	function updatePeriod_post(){
        log_message('INFO','Incoming request on updatePeriod_post. From IP '.$_SERVER['REMOTE_ADDR']);
    	
		log_message('INFO','Incoming request on updatePeriod_post. data = "'.','.$this->post('PERIOD_ID').','.$this->post('DESCRIPTION').','.$this->post('FROM_DATE').','.$this->post('UNTIL_DATE').','.$this->post('ROWVERSION').'"');
		$data = $this->Periodmodel->updatePeriod($this->post('PERIOD_ID'),$this->post('DESCRIPTION'),$this->post('FROM_DATE'),$this->post('UNTIL_DATE'),$this->post('ROWVERSION'));
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'Data Cannot Be Insert'), 404);
		}
	}
	
	function deletePeriod_post(){
        log_message('INFO','Incoming request on deletePeriod_post. From IP '.$_SERVER['REMOTE_ADDR']);
    	
		log_message('INFO','Incoming request on deletePeriod_post. data = "'.$this->post('PERIOD_ID').'"');
		$data = $this->Periodmodel->deletePeriod($this->post('PERIOD_ID'));
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'Data Cannot Be Insert'), 404);
		}
	}
	
	function getPeriodList_post(){
        log_message('INFO','Incoming request on getPeriodList_post. From IP '.$_SERVER['REMOTE_ADDR']);
		
		$data = $this->Periodmodel->getPeriodList($this->post('start'),$this->post('limit'));
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'Data Cannot Be Insert'), 404);
		}
	}
	
	function getPeriodCurrentData_post(){
        log_message('INFO','Incoming request on getPeriodCurrentData_post. From IP '.$_SERVER['REMOTE_ADDR']);
		
		$data = $this->Periodmodel->getPeriodCurrentData($this->post('PERIOD_ID'));
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'Data Cannot Be Insert'), 404);
		}
	}
	
	function validatePeriod_post(){
        log_message('INFO','Incoming request on validatePeriod_post. From IP '.$_SERVER['REMOTE_ADDR']);
		
		$data = $this->Periodmodel->validatePeriod($this->post('PERIOD_ID'),$this->post('DATE_TO_VALIDATE'));
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'Data Cannot Be Insert'), 404);
		}
	}
	
	function getMapping_post(){
        log_message('INFO','Incoming request on getMapping_post. From IP '.$_SERVER['REMOTE_ADDR']);
		
		$data = $this->Mappingmodel->getMappingList($this->post('SUB_DIST_MAP_VALUE'),$this->post('MAPPING_ID'),$this->post('SUB_DIST_ID'));
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'Data could not be found'), 404);
		}
		
	}
	
	function getMappingCurrentData_post(){
        log_message('INFO','Incoming request on getMappingCurrentData_post. From IP '.$_SERVER['REMOTE_ADDR']);
		
		$data = $this->Mappingmodel->getMappingCurrent($this->post('SUB_DIST_ID'),$this->post('SUB_DIST_MAP_VALUE'));
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'Data could not be found'), 404);
		}
		
	}
	
	function insertPeriodStatus_post(){
        log_message('INFO','Incoming request on insertPeriodStatus_post. From IP '.$_SERVER['REMOTE_ADDR']);
    	
		log_message('INFO','Incoming request on insertPeriodStatus_post. data = "'.$this->post('PERIOD_ID').','.$this->post('SUB_DIST_ID').','.$this->post('REPORT_TYPE').','.$this->post('SUBMIT_DATE').','.$this->post('SUBMIT_BY').','.$this->post('ROWVERSION').'"');
		$data = $this->Periodstatusmodel->insertPeriodStatus($this->post('PERIOD_ID'),$this->post('SUB_DIST_ID'),$this->post('REPORT_TYPE'),$this->post('SUBMIT_DATE'),$this->post('SUBMIT_BY'),$this->post('ROWVERSION'));
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'Data Cannot Be Insert'), 404);
		}
	}
	
	function checkPeriodStatus_post(){
        log_message('INFO','Incoming request on checkPeriodStatus_post. From IP '.$_SERVER['REMOTE_ADDR']);
		
		$data = $this->Periodstatusmodel->checkData($this->post('PERIOD_ID'),$this->post('SUB_DIST_ID'),$this->post('REPORT_TYPE'));
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'Data could not be found'), 404);
		}
		
	}
	
	function getMappingMstList_get(){
        log_message('INFO','Incoming request on getMappingMstList_post. From IP '.$_SERVER['REMOTE_ADDR']);
		
		$data = $this->Mappingmodel->getMappingMstList();
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'Data could not be found'), 404);
		}
		
	}
	
	function getSubdistMstList_get(){
        log_message('INFO','Incoming request on getSubdistMstList_post. From IP '.$_SERVER['REMOTE_ADDR']);
		
		$data = $this->Subdistmodel->getSubdistMstList();
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'Data could not be found'), 404);
		}
		
	}
	
	function getMappingList_post(){
        log_message('INFO','Incoming request on getMappingList_post. From IP '.$_SERVER['REMOTE_ADDR']);
		
		$data = $this->Mappingmodel->getMappingListView($this->post('start'),$this->post('limit'));
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'Data Cannot Be Insert'), 404);
		}
	}
	
	function insertMapping_post(){
        log_message('INFO','Incoming request on insertMapping_post. From IP '.$_SERVER['REMOTE_ADDR']);
    	
		log_message('INFO','Incoming request on insertMapping_post. data = "'.','.$this->post('MAPPING_ID').','.$this->post('SUB_DIST_ID').','.$this->post('SUB_DIST_MAP_VALUE').','.$this->post('OUR_MAP_VALUE').','.$this->post('ROWVERSION').'"');
		$data = $this->Mappingmodel->insertMapping($this->post('MAPPING_ID'),$this->post('SUB_DIST_ID'),$this->post('SUB_DIST_MAP_VALUE'),$this->post('OUR_MAP_VALUE'),$this->post('ROWVERSION'));
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'Data Cannot Be Insert'), 404);
		}
	}
	
	function updateMapping_post(){
        log_message('INFO','Incoming request on updateMapping_post. From IP '.$_SERVER['REMOTE_ADDR']);
    	
		log_message('INFO','Incoming request on updateMapping_post. data = "'.','.$this->post('MAPPING_ID').','.$this->post('SUB_DIST_ID').','.$this->post('SUB_DIST_MAP_VALUE').','.$this->post('OUR_MAP_VALUE').','.$this->post('ROWVERSION').'"');
		$data = $this->Mappingmodel->updateMapping($this->post('MAPPING_ID'),$this->post('SUB_DIST_ID'),$this->post('SUB_DIST_MAP_VALUE'),$this->post('OUR_MAP_VALUE'),$this->post('ROWVERSION'));
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'Data Cannot Be Insert'), 404);
		}
	}
	
	function deleteMapping_post(){
        log_message('INFO','Incoming request on deleteMapping_post. From IP '.$_SERVER['REMOTE_ADDR']);
    	
		log_message('INFO','Incoming request on deleteMapping_post. data = "'.','.$this->post('MAPPING_ID').','.$this->post('SUB_DIST_ID').','.$this->post('SUB_DIST_MAP_VALUE').','.$this->post('OUR_MAP_VALUE').'"');
		$data = $this->Mappingmodel->deleteMapping($this->post('MAPPING_ID'),$this->post('SUB_DIST_ID'),$this->post('SUB_DIST_MAP_VALUE'),$this->post('OUR_MAP_VALUE'));
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'Data Cannot Be Insert'), 404);
		}
	}

	function getPeriodReport_post(){
        log_message('INFO','Incoming request on getPeriodReport_post. From IP '.$_SERVER['REMOTE_ADDR']);
		
		$data = $this->Periodmodel->getPeriodReport($this->post('month'),$this->post('year'));
        if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
        else {
            $this->response(array('error' => 'Data Cannot Be Insert'), 404);
		}
	}
	
	//========================================= DEFAULT =======================================//
    function user_post()
    {
        //$this->some_model->updateUser( $this->get('id') );
        $message = array('id' => $this->get('id'), 'name' => $this->post('name'), 'email' => $this->post('email'), 'message' => 'ADDED!');
        
        $this->response($message, 200); // 200 being the HTTP response code
    }
    
    function user_delete()
    {
    	//$this->some_model->deletesomething( $this->get('id') );
        $message = array('id' => $this->get('id'), 'message' => 'DELETED!');
        
        $this->response($message, 200); // 200 being the HTTP response code
    }
    
    function users_get()
    {
        //$users = $this->some_model->getSomething( $this->get('limit') );
        $users = array(
			array('id' => 1, 'name' => 'Some Guy', 'email' => 'example1@example.com'),
			array('id' => 2, 'name' => 'Person Face', 'email' => 'example2@example.com'),
			3 => array('id' => 3, 'name' => 'Scotty', 'email' => 'example3@example.com', 'fact' => array('hobbies' => array('fartings', 'bikes'))),
		);
        
        if($users)
        {
            $this->response($users, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Couldn\'t find any users!'), 404);
        }
    }



	public function send_post()
	{
		var_dump($this->request->body);
	}


	public function send_put()
	{
		var_dump($this->put('foo'));
	}
}