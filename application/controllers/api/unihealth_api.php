<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Example
*
* This is an example of a few basic user interaction methods you could use
* all done with a hardcoded array.
*
* @package		CodeIgniter
* @subpackage	Rest Server
* @category	Controller
* @author		Phil Sturgeon
* @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Unihealth_api extends REST_Controller
{
  public function __Construct()
  {
    parent::__Construct();
    //constructor code
    //$this->load->model('Testmodel');
    /*
    $this->load->model('Usermodel');
    $this->load->model('Customerreport');
    $this->load->model('Inventoryreport');
    $this->load->model('Transactionreport');
    $this->load->model('Periodmodel');
    $this->load->model('Mappingmodel');
    $this->load->model('Periodstatusmodel');
    $this->load->model('Subdistmodel');
    */
    $this->load->model('api/Itemmodel');
    $this->load->model('api/Vendormodel');
    $this->load->model('api/Pomodel');
  }

  function insertTest_post(){
    //$this->load->model('Usermodel');
    log_message('INFO','Incoming request on insertTest_post. From IP '.$_SERVER['REMOTE_ADDR']);
    log_message('INFO','Incoming request on insertTest_post. data = "'.(string)$this->post('items').'". From IP '.$_SERVER['REMOTE_ADDR']);

    $getResult = json_decode((string)$this->post('items'));
    //echo $getResult;
    $this->response($getResult, 200); // 200 being the HTTP response code


    //$data = $this->Testmodel->insertCust($this->post('PERIOD_ID'),$this->post('SUB_DIST_ID'),$this->post('SUBMIT_DATE'));
    /*
    if($data){
    $this->response($data, 200);} // 200 being the HTTP response code
    else {
    $this->response(array('error' => 'Data Cannot Be Insert'), 404);
  }
  */
}
//========================================= DEFAULT =======================================//
function user_post()
{
  //$this->some_model->updateUser( $this->get('id') );
  $message = array('id' => $this->get('id'), 'name' => $this->post('name'), 'email' => $this->post('email'), 'message' => 'ADDED!');

  $this->response($message, 200); // 200 being the HTTP response code
}

function user_delete()
{
  //$this->some_model->deletesomething( $this->get('id') );
  $message = array('id' => $this->get('id'), 'message' => 'DELETED!');

  $this->response($message, 200); // 200 being the HTTP response code
}

function users_get()
{
  //$users = $this->some_model->getSomething( $this->get('limit') );
  $users = array(
    array('id' => 1, 'name' => 'Some Guy', 'email' => 'example1@example.com'),
    array('id' => 2, 'name' => 'Person Face', 'email' => 'example2@example.com'),
    3 => array('id' => 3, 'name' => 'Scotty', 'email' => 'example3@example.com', 'fact' => array('hobbies' => array('fartings', 'bikes'))),
  );

  if($users)
  {
    $this->response($users, 200); // 200 being the HTTP response code
  }

  else
  {
    $this->response(array('error' => 'Couldn\'t find any users!'), 404);
  }
}



public function send_post()
{
  var_dump($this->request->body);
}


public function send_put()
{
  var_dump($this->put('foo'));
}

// By Annisa Rahmawaty 2019
function materialgimmick_post(){

        //$this->load->model('Usermodel');
        $this->load->model('Itemmodel');  
        $entityBody = file_get_contents('php://input', 'r');
        $bodyDecode = json_decode((string)$entityBody); // terima dari SAP
        //$getResult = json_decode((string)$this->post('items')); // terima dari sohomlm_push

        
        log_message('INFO','Incoming request on materialGimmick_post. From IP '.$_SERVER['REMOTE_ADDR']);
        log_message('INFO','Incoming request on materialGimmick_post. data = "'.json_encode($bodyDecode).'","'.json_encode($entityBody).'". From IP '.$_SERVER['REMOTE_ADDR']);
        
        //print_r($bodyDecode);


       // $this->response($bodyDecode, 200); // 200 being the HTTP response code

        //$this->response($getResult);

        if($bodyDecode){
            //$this->response($bodyDecode, 200);} // 200 being the HTTP response code
          foreach ($bodyDecode as $bd) {
            $item_id = $bd->MATNR;
            $name    = $bd->MAKTX;
            $satuan  = $bd->MEINS;
            $hpp     = str_replace(',', '', $bd->YHJP); //$bd->YHJP;
			$price	 = str_replace(',', '', $bd->YHNA_EAST);
			$price2	 = str_replace(',', '', $bd->YHNA_WEST);
            $sales   = $bd->ACTIVE;
			if($sales == 'X') $sales = 'Yes'; else $sales = 'No';
            $created = $bd->YDATE;
            $createdby  = $bd->YUSER;
            $material   = $bd->MATKL;
            $purchasing = $bd->EKGRP;
            $updateby = $bd->YUSER_CHANGES;
            $updated  = $bd->YDATE_CHANGES;
            $time     = $bd->YHOUR;
            $timec    = $bd->YHOUR_CHANGES;
            //$this->db->get('item');
            //echo "hpp == ".$hpp;
            //$createdf = date('Y-m-d',strtotime($created)).' '.$time;
			/*
            $date = new DateTime($created);
            $createdf = $date->format('Y-m-d').' '.$time;
			$dateupdate = new DateTime($updated);
            $updatedf = $dateupdate->format('Y-m-d').' '.$timec;
			*/
            $date = new DateTime($created.$time);
            $createdf = $date->format('Y-m-d H:i:s');
			$dateupdate = new DateTime($updated.$timec);
            $updatedf = $dateupdate->format('Y-m-d H:i:s');
            //echo $createdf;
            //$data = $this->Itemmodel->insertitem($item_id,$name,$satuan,$hpp,$sales,$createdf,$createdby,$material,$purchasing);
            $data = $this->Itemmodel->insertitem($item_id,$name,$satuan,$hpp,$sales,$createdf,$createdby,$material,$purchasing,$updatedf,$updateby,$price,$price2);
    
            if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
            else {
            $this->response(array('error' => 'Data Cannot Be Insert'), 404);
          }
          }
        }else {
            $this->response(array('error' => 'Data Cannot Be Insert'), 404);
        }
        /*
        
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
        */

    }

function vendor_post(){
 
        $entityBody = file_get_contents('php://input', 'r');
        $bodyDecode = json_decode((string)$entityBody); // terima dari SAP
        //$getResult = json_decode((string)$this->post('items')); // terima dari sohomlm_push

        
        log_message('INFO','Incoming request on vendor_post. From IP '.$_SERVER['REMOTE_ADDR']);
        log_message('INFO','Incoming request on vendor_post. data = "'.json_encode($bodyDecode).'","'.json_encode($entityBody).'". From IP '.$_SERVER['REMOTE_ADDR']);
        
        //print_r($bodyDecode);

        if($bodyDecode){
            //$this->response($bodyDecode, 200);} // 200 being the HTTP response code
          foreach ($bodyDecode as $bd) {
            $account  = $bd->LIFNR;
            $country  = $bd->LAND1;
            $name1    = $bd->NAME1;
            $kode_pos = $bd->PSTLZ;
            $add      = $bd->STRAS;
            $num      = $bd->ADRNR;
            $title    = $bd->ANRED;
            $created  = $bd->ERDAT;
            $createdby= $bd->ERNAM;
            $telp     = $bd->TELF1;
            $npwp     = $bd->STCEG;
                        
            $alamat = $add.' '.$num.' '.$kode_pos.' '.$country;
            $name   = $title.' '.$name1;
            $createdf = date('Y-m-d',strtotime($created));
            //echo $alamat;
            
            $data = $this->Vendormodel->insertVendor($account,$name,$alamat,$telp,$createdf,$createdby,$npwp);
    
            if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
            else {
            $this->response(array('error' => 'Data Cannot Be Insert'), 404);
            }
          }
        }else {
            $this->response(array('error' => 'Data Cannot Be Insert'), 404);
        }

    }

    function po_post(){
 
        $entityBody = file_get_contents('php://input', 'r');
        $bodyDecode = json_decode((string)$entityBody); // terima dari SAP
        //$getResult = json_decode((string)$this->post('items')); // terima dari sohomlm_push

        
        log_message('INFO','Incoming request on po_post. From IP '.$_SERVER['REMOTE_ADDR']);
        log_message('INFO','Incoming request on po_post. data = "'.json_encode($bodyDecode).'","'.json_encode($entityBody).'". From IP '.$_SERVER['REMOTE_ADDR']);
        
        //print_r($bodyDecode);

        if($bodyDecode){
            //$this->response($bodyDecode, 200);} // 200 being the HTTP response code
          foreach ($bodyDecode as $bd) {
            $po_id      = $bd->EBELN;
            $tanggal    = $bd->AEDAT;
            $supplier   = $bd->LIFNR;
            $line       = $bd->EBELP;
            $wh         = $bd->WERKS;
            $sloc       = $bd->LGORT;
            $item_id    = $bd->MATNR;
            $name       = $bd->TXZ01;
            $qty        = $bd->MENGE;
            $price      = str_replace(',', '', $bd->NETPR);
            $subtotal   = str_replace(',', '', $bd->NETWR);
            $createdby  = $bd->ERNAM;
            $deletion   = $bd->LOEKZ;
			
            
            //$createdf = date('Y-m-d',strtotime($tanggal));
            $date = new DateTime($tanggal);
            $createdf = $date->format('Y-m-d');
            $createdftime = $createdf.' '.date('H:i:s');
            //echo $po_id;
			
            if ($deletion != 'L') {
              //$data = $this->Pomodel->insertPodel($po_id,$createdf,$supplier,$wh,$sloc,$createdby);
            //}else{
              $data = $this->Pomodel->insertPo($po_id,$createdf,$supplier,$line,$wh,$item_id,$qty,$price,$subtotal,$sloc,$createdby,$createdftime);
            }
         }
			 if($data){
				$this->response($data, 200);} // 200 being the HTTP response code
			 else {
				$this->response(array('error' => 'Data Cannot Be Insert 01'), 404);
			 }

        }else {
            $this->response(array('error' => 'Data Cannot Be Insert 02'), 404);
        }

    }

    function actualpo_post(){
 
        $entityBody = file_get_contents('php://input', 'r');
        $bodyDecode = json_decode((string)$entityBody); // terima dari SAP
        //$getResult = json_decode((string)$this->post('items')); // terima dari sohomlm_push

        
        log_message('INFO','Incoming request on po_post. From IP '.$_SERVER['REMOTE_ADDR']);
        log_message('INFO','Incoming request on po_post. data = "'.json_encode($bodyDecode).'","'.json_encode($entityBody).'". From IP '.$_SERVER['REMOTE_ADDR']);
        
        //print_r($bodyDecode);

        if($bodyDecode){
            //$this->response($bodyDecode, 200);} // 200 being the HTTP response code
          foreach ($bodyDecode as $bd) {
			$gr_id 		= $bd->MBLNR;
            $po_id      = $bd->EBELN;
            $tahun      = $bd->MJAHR;
            $ed         = date('Y-m-d',strtotime($bd->VFDAT));
            $supplier   = $bd->LIFNR;
            $line       = $bd->ZEILE;//LINE_ID;
            $line_po    = $bd->EBELP;
            $plan       = $bd->WERKS;
            $sloc       = $bd->LGORT;
            $item_id    = $bd->MATNR;
            $name       = $bd->SGTXT;
            $qty        = $bd->ERFMG;
            $price      = str_replace(',', '', $bd->DMBTR);
            $createdby  = $bd->UNAME;//USNAM_MKP;
            $createdf   =  date('Y-m-d',strtotime($bd->BUDAT));// date('Y-m-d',strtotime($bd->BUDAT_MKPF));
            //echo $po_id;
            $date = new DateTime($bd->BUDAT);//new DateTime($bd->BUDAT_MKPF);
            $createdf = $date->format('Y-m-d');
            $createdftime = $createdf.' '.date('H:i:s');
			$subtotal = $price*$qty; 
          
              $data = $this->Pomodel->insertActualPo($po_id,$line,$supplier,$plan,$sloc,$item_id,$name,$qty,$price,$createdby,$createdf,$createdftime,$subtotal,$gr_id,$tahun);
         }
            if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
            else {
            $this->response(array('error' => 'Data Cannot Be Insert 01'), 404);
            }

        }else {
            $this->response(array('error' => 'Data Cannot Be Insert 02'), 404);
        }

    }

    // ---------- end Annisa Rahmawaty 2019 --------
	
	function hna_post(){
 
        $entityBody = file_get_contents('php://input', 'r');
        $bodyDecode = json_decode((string)$entityBody); // terima dari SAP
        //$getResult = json_decode((string)$this->post('items')); // terima dari sohomlm_push

        
        log_message('INFO','Incoming request on vendor_post. From IP '.$_SERVER['REMOTE_ADDR']);
        log_message('INFO','Incoming request on vendor_post. data = "'.json_encode($bodyDecode).'","'.json_encode($entityBody).'". From IP '.$_SERVER['REMOTE_ADDR']);
        
        //print_r($bodyDecode);

        if($bodyDecode){
            //$this->response($bodyDecode, 200);} // 200 being the HTTP response code
          foreach ($bodyDecode as $bd) {
            $VKORG  	= $bd->VKORG;
            $VTWEG  	= $bd->VTWEG;
            $REGIO     	= $bd->REGIO;
            $item_id   	= $bd->MATNR;
            $item_desc 	= $bd->MAKTG;
            $price     	= $bd->KBETR;
            $validfrom 	= $bd->DATAB;
            $validto  	= $bd->DATBI;
            $updatedby 	= $bd->USNAM;
                        
            $createdf = date('Y-m-d',strtotime($validfrom));
            //echo $alamat;
            
            $data = $this->Itemmodel->updatehna($item_id,$price,$validfrom,$REGIO,$updatedby);
    
            if($data){
            $this->response($data, 200);} // 200 being the HTTP response code
            else {
            $this->response(array('error' => 'Data Cannot Be Insert'), 404);
            }
          }
        }else {
            $this->response(array('error' => 'Data Cannot Be Insert'), 404);
        }

    }
}
